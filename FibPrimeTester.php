<?php
/**
 * Created by PhpStorm.
 * User: mtimmons
 * Date: 6/16/17
 * Time: 2:15 PM
 */
require_once('FibPrimeEcho.php');

function print_usage()
{
    echo "Usage:  php FibPrimeTester <n> where n is integer\n";

}


if ($argc < 2 )
{
    print_usage();
    exit();
}
$n = $argv[1];

echo "Welcome to Fibonocci echo and Prime tester\n";

if (FibPrimeEcho::IsDivBy($n, 15))
{
    echo "Buzz...your value is divisible by 3\n";
    echo "Fizz...your value is divisible by 5\n";
    echo "FizzBuzz...your value is divisible by 15\n";
}
else if (FibPrimeEcho::IsDivBy($n, 5))
{
    echo "Fizz...your value is divisible by 5\n";
}
else if (FibPrimeEcho::IsDivBy($n, 3))
{
    echo "Buzz...your value is divisible by 3\n";
}
else if(FibPrimeEcho::IsPrime($n))
{
    echo "BuzzFizz..." . $n . " is prime\n";
}
else
{
    $fibs = FibPrimeEcho::Fibonacci($n);

    echo 'F(' , $n . ') = ';
    foreach($fibs as $fib)
    {
        echo $fib . ",";
    }
    echo "\n";
}