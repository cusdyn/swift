<?php

/**
 * Created by PhpStorm.
 * User: mtimmons
 * Date: 6/16/17
 * Time: 2:14 PM
 */
class FibPrimeEcho
{

    /**
     * FibPrimeEcho constructor.
     * @param $n
     */
    function __construct()
    {
    }

    /**
     * @param $n
     */
    function IsPrime($n)
    {
        for($i=2; $i<$n;$i++)
        {
            if($n%$i==0)
                return false;
        }
        return true;
    }

    /**
     * @param $n
     * @param $divisor
     */
    function IsDivBy($n, $divisor)
    {
        return !($n%$divisor);
    }

    function Fibonacci($n)
    {
        if ($n == 0) {
            $v = [0];
        } else {
            $v = [0, 1];
            for ($i = 1; $i < $n; $i++) {
                $x = $v[$i] + $v[$i - 1];
                if (!is_infinite($x)){
                    $v[] = $x;
                }
                else
                {
                    $v[] = "N TOO BIG...cannot compute past F(" . $i . ")\n";
                    break;
                }
            }
        }
        return $v;
    }
}